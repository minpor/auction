**Проект аукцион рекламы**

Проект делится на 2 части

* `front` - фронт энд, написанный на angular2 (обязательная)
* `back-end` - бэк энд, написанный на java и представляет из себя rest сервисы для взаимодействия с `front` (обязательная)

Описание бизнес процессов в папке bpmn/

**Запуск приложения**

* Настраиваем конфигурационный файл `back-end/src/main/resources/application.properties`. Для работы необходим PostgreSQL 9.5 и выше
* Запускаем `back-end/src/main/java/ru/market/BackEndApplication.java` как java приложение
* Переходим в каталог front и запускаем команду `npm install` (желательно удалить старые библиотеки)
* Запускаем команду `ng serve`
* Переходим по ссылке `http://localhost:4200/`

**Документация**

Документация лежит в папках `documents` и `bpmn`

**Прочее**

Репозиторий шаблона:
https://github.com/mrholek/Leaf-Bootstrap-4-Admin-Template-with-AngularJS-Angular-2-support

**Полезные ссылки**

liquibase http://sergeydanilov.blogspot.ru/2015/05/liquibase-gradle.html
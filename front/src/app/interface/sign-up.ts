export interface SignUp {
    username: string;
    email: string;
    name: string;
    role: string;
    password: string;
    confirmPassword: string;
}

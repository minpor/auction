import { NgModule }             from '@angular/core';
import { Routes,
         RouterModule }         from '@angular/router';

import { StatComponent }   from './stat.component';

const routes: Routes = [
    {
        path: '',
        component: StatComponent,
        data: {
            title: 'Статистика'
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class StatRoutingModule {}

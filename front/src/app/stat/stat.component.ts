import { Component, OnInit }    from '@angular/core';
import { UserService } from '../service/user.service'
import { User } from '../model/user'
import {Http, Headers} from '@angular/http';

@Component({
    templateUrl: 'stat.component.html',
    providers: [UserService]
})
export class StatComponent implements OnInit {

    constructor(private userService: UserService) {
    
    }

    ngOnInit(): void {
    }
}

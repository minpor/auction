import { NgModule }                 from '@angular/core';
import { ChartsModule }             from 'ng2-charts/ng2-charts';
import {HttpModule} from '@angular/http';

import { StatComponent }       from './stat.component';
import { StatRoutingModule }   from './stat-routing.module';



@NgModule({
    imports: [
        StatRoutingModule,
        ChartsModule,
        HttpModule
    ],
    declarations: [ StatComponent ]
})
export class StatModule { }


import {Component, OnInit, Input, OnChanges} from "@angular/core";
import {Auction} from "../../model/auction";
import {Tender} from "../../model/tender";

@Component({
    selector: 'auction-info',
    templateUrl: './auction-info.component.html',
    styleUrls: ['auction-info.style.css']
})
export class AuctionInfoComponent implements OnInit, OnChanges {

    @Input() auction:Auction;
    @Input() tenderList:Tender[];
    tenderBatchList:Tender[];
    hasTender:boolean;

    public tenderCurrentPage:number = 1;
    public tenderTotalCount:number; // количество записей (всего)
    public tenderPageSize:number = 5; // количество записей (на странице)

    constructor() {
    }

    ngOnInit() {
    }

    ngOnChanges() {
        this.tenderTotalCount = this.tenderList.length;
        this.hasTender = this.tenderList.length > 0;
        if (this.tenderTotalCount < this.tenderPageSize) {
            this.tenderTotalCount = this.tenderPageSize;
        }
        this.tenderPagination();
    }

    tenderEventClick(event:any):number {
        this.tenderCurrentPage = event;
        this.tenderPagination();
        return event;
    }

    tenderPagination() {
        if (this.tenderTotalCount == this.tenderPageSize) {
            this.tenderBatchList = this.tenderList;
        } else {
            const rightValue = Math.imul(this.tenderPageSize, this.tenderCurrentPage);
            const leftValue = rightValue - this.tenderPageSize - 1;
            this.tenderBatchList = this.tenderList.slice(leftValue, rightValue);
        }
    }

    fillUserInfo() {

    }

}

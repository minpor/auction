import {Component, OnInit, ViewChild, Input, Output, EventEmitter, DoCheck} from "@angular/core";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ModalDirective} from "ng2-bootstrap";
import {AuctionService} from "../../service/auction.service";
import {Tender} from "../../model/tender";
import {Cookie} from "ng2-cookies/ng2-cookies";

import createNumberMask from 'text-mask-addons/dist/createNumberMask';
import {StringUtils} from "../../common/string-utils";
import {UserUtils} from '../../common/user-utils';

@Component({
    selector: 'app-rate',
    templateUrl: './rate.component.html',
    providers: [AuctionService]
})
export class RateComponent implements OnInit, DoCheck {

    public priceMask = createNumberMask({prefix: '', thousandsSeparatorSymbol: ' '});

    @Input() auctionId:number;
    @Input() tenderList:Tender[] = [];
    @Output() update:EventEmitter<Tender[]> = new EventEmitter<Tender[]>();

    @ViewChild('childModal') public childModal:ModalDirective;

    form:FormGroup;
    formBuilder:FormBuilder;
    service:AuctionService;

    userId:number = +Cookie.get("userId");

    listChanged:boolean = false;
    rateButtonDisable:boolean = false;

    validForm:boolean;
    userUtils:UserUtils = new UserUtils();

    constructor(formBuilder:FormBuilder,
                private auctionService:AuctionService) {
        this.form = formBuilder.group({
            place: ['', Validators.required],
            price: ['', Validators.required]
        });
        this.service = auctionService;
        this.formBuilder = formBuilder;
        this.rateButtonDisable = false;

        this.form.valueChanges.subscribe(data => {
            this.validForm = this.form.valid;
        });
    }

    ngDoCheck() {
        if (this.listChanged) {
            this.update.emit(this.tenderList);
            this.rateButtonDisable = false;
        }
        this.listChanged = false;
    }


    public showChildModal():void {
        this.childModal.show();
    }

    public hideChildModal():void {
        this.childModal.hide();
    }

    ngOnInit() {}

    public createTender() {
        let val = this.form.value;
        let tender:Tender = new Tender();
        tender.place = val.place;
        tender.price = new StringUtils().clearSpaces(val.price);
        tender.userId = +localStorage.getItem('userId');
        tender.auctionId = this.auctionId;
        this.auctionService.createTenderService(tender).subscribe(
            (resp)=> {
                this.tenderList.concat(resp);
                this.listChanged = true;
            }
        );
        this.form = this.formBuilder.group({
            place: [""],
            price: [""]
        });

    }

    public isValid():boolean {
        return !this.validForm;
    }

}

import { NgModule }             from '@angular/core';
import { Routes, RouterModule }         from '@angular/router';

import { AuctionNewComponent }   from './auction-new/auction-new.component';
import { AuctionListComponent }   from './auction-list/auction-list.component';



const routes: Routes = [
    {
        path: '',
        component: AuctionNewComponent,
        data: {
            title: 'Аукцион'
        }
    },
    {
        path: 'list',
        component: AuctionListComponent,
        data: {
            title: 'Список торгов'
        }
    },
    {
        path: 'list/:body',
        component: AuctionListComponent,
        data: {
            title: 'Список торгов'
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AuctionRoutingModule {}

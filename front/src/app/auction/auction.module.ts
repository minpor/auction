import { NgModule }                 from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AuctionNewComponent }       from './auction-new/auction-new.component';
import { AuctionListComponent }       from './auction-list/auction-list.component';
import { AuctionRoutingModule }   from './auction-routing.module';

import { TabsModule, ModalModule }               from 'ng2-bootstrap';
import {AuctionInfoComponent} from "./auction-info/auction-info.component";
import {RateComponent} from "./modals/rate.component";

import { Ng2PaginationModule } from 'ng2-pagination';
import { InputFormatDirective } from './input-format.directive';

import { TextMaskModule } from 'angular2-text-mask';
import { NumberFormatterPipe } from "../pipe/number-formatter.pipe";
import { BooleanToWordPipe } from '../pipe/boolean-to-word.pipe';

@NgModule({
    imports: [
        TabsModule.forRoot(),
        AuctionRoutingModule,
        HttpModule,
        FormsModule,
        ReactiveFormsModule,
        CommonModule,
        ModalModule.forRoot(),
        Ng2PaginationModule,
        TextMaskModule
    ],
    declarations: [
        AuctionNewComponent,
        AuctionListComponent,
        AuctionInfoComponent,
        RateComponent,
        InputFormatDirective,
        NumberFormatterPipe,
        BooleanToWordPipe
    ],
    exports: [AuctionNewComponent],
    bootstrap: [AuctionListComponent]
})
export class AuctionModule { }


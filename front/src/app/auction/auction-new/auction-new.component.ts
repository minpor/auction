import {Component, OnInit} from "@angular/core";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AuctionService} from "../../service/auction.service";
import {Auction} from "../../model/auction";
import {AuctionFilter} from "../../model/auctionFilter";
import {Router} from "@angular/router";

import createNumberMask from 'text-mask-addons/dist/createNumberMask'
import {StringUtils} from "../../common/string-utils";

@Component({
    selector: 'auction-form-create',
    templateUrl: 'auction-new.component.html',
    providers: [AuctionService]
})
export class AuctionNewComponent implements OnInit {

    public priceMask = createNumberMask({prefix: '', thousandsSeparatorSymbol: ' '});
    
    public form:FormGroup;
    validForm:boolean;
    auction:Auction = new Auction();
    service:AuctionService;

    private id:number;
    private statusCreateButton:boolean = false;
    private hideInfo:boolean = true;
    
    public filterById:AuctionFilter;

    constructor(private formBuilder:FormBuilder,
                private auctionService:AuctionService,
                private router: Router ) {
        this.service = auctionService;
    }

    ngOnInit():void {
        this.buildForm();
    }

    buildForm(): void {
        this.form = this.formBuilder.group({
            locality: ['', Validators.required],
            district: ['', Validators.required],
            days: ['', Validators.required],
            budget: ['', Validators.required],
            size: [],
            light: ['', Validators.required]
        });

        this.form.valueChanges.subscribe(data => {
            this.validForm = this.form.valid;
        });
    }

    public isValid():boolean {
        return !this.validForm || this.statusCreateButton;
    }

    createAuction() {

        let val = this.form.value;

        this.auction.userId = Number(localStorage.getItem('userId'));
        this.auction.auctionType = 0; // нет поддержки другого типа аукциона
        this.auction.locality = val.locality;
        this.auction.district = val.district;
        this.auction.days = val.days;
        this.auction.budget = new StringUtils().clearSpaces(val.budget);
        this.auction.size = val.size;
        this.auction.light = val.light;

        this.auctionService.auctionCreateService(this.auction).subscribe(
            (resp)=> {
                this.id = resp.id;
                this.hideInfo = false;
            }
        );

        this.statusCreateButton = true;

    }
    
    auctionListRedirect() {
        this.filterById = new AuctionFilter();
        this.filterById.id = this.id;
        this.router.navigate(['/auction/list', this.filterById]);
    }


    
}

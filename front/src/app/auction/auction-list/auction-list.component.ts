import { Component, OnInit, OnDestroy } from "@angular/core";
import {Http} from "@angular/http";
import {FormBuilder, FormGroup} from "@angular/forms";
import {AuctionService} from "../../service/auction.service";
import {AuctionFilter} from "../../model/auctionFilter";
import {Auction} from "../../model/auction";
import {ActivatedRoute, Router} from "@angular/router";
import {VariablesService} from "../../service/variables.service";
import {Tender} from "../../model/tender";
import {UserService} from "../../service/user.service";
import {Pagination} from "../../model/pagination";

@Component({
    selector: 'auction-form-list',
    templateUrl: 'auction-list.component.html',
    styleUrls: ['auction-list.style.css'],
    providers: [AuctionService, UserService, VariablesService]
})
export class AuctionListComponent implements OnInit, OnDestroy {

    form:FormGroup;
    auctionFilter:AuctionFilter = new AuctionFilter();
    auctionList:Auction[] = [];
    auction:Auction = new Auction();
    tenderList:Tender[] = [];
    auctionService:AuctionService;
    variablesService:VariablesService = new VariablesService();
    userService:UserService;

    hideAuctionInfo:boolean = true;
    hideAuctionList:boolean = true;
    hideFilter:boolean = false;

    filterIco:string = "icon-arrow-up";

    private sub:any;
    id:number;

    selectedRow:number;
    setClickedRow:Function;

    activeTab:number;
    hasTender:boolean;
    customSearch:boolean = true;

    //дефолтные значения для пагинации
    public currentPage:number = 1;
    public totalCount:number = 20; // количество записей (всего)
    public pageSize:number = 5; // количество записей (на странице)

    constructor(formBuilder:FormBuilder,
                private route:ActivatedRoute,
                private http:Http,
                private router:Router) {
        this.form = formBuilder.group({
            input: [],
            id: [],
            locality: [],
            district: [],
            days: [],
            dateFrom: [],
            dateTo: [],
            budget: [],
            size: [],
            light: []
        });
        this.auctionService = new AuctionService(http);
        this.userService = new UserService(http);
        this.setClickedRow = function (index) {
            this.selectedRow = index;
            this.auction = this.auctionList[this.selectedRow];
            this.updateRate();
            
            this.hideAuctionInfo = false;
        };
    }

    onUpdate(event) {
        this.tenderList = event;
        this.updateRate();
    }

    ngOnInit():void {
        this.sub = this.route.params.subscribe(params => {
            if (Object.keys(params).length != 0) {
                this.auctionFilter.id = (<AuctionFilter>params).id;
                const id = this.auctionFilter.id;
                this.id = id;
                this.form.value.id = id;
                this.currentPage = 1;
                this.pageSize = 1;
                this.filter();
            }
        });
        this.selectedRow = -1;

    }

    ngOnDestroy() {
        this.sub.unsubscribe();
    }

    eventClick(event:any):number {
        this.currentPage = event;
        this.filter();
        return event;
    }

    filter() {
        this.pageSize = 5;
        this.selectedRow = -1;
        this.hideAuctionInfo = true;

        const val = this.form.value;
        this.auctionFilter.input = val.input;
        this.auctionFilter.id = val.id;
        this.auctionFilter.locality = val.locality;
        this.auctionFilter.district = val.district;
        
        const pagination = new Pagination();
        pagination.limit = this.pageSize;
        pagination.page = this.currentPage;

        this.auctionFilter.pagination = pagination;
        
        this.getPagesCount();
        this.auctionService.auctionFilterService(this.auctionFilter).subscribe(
            (res)=> {
                this.auctionList = res;
            }
        );
        this.auction.id = 0;
        this.updateRate();
        this.hideAuctionList = false;
    }
    
    getPagesCount() {
        this.auctionService.auctionPagesCountFilterService(this.auctionFilter).subscribe((resp) => {
            this.totalCount  = resp;
        });
    }

    filterMaximizeMinimize() {
        this.hideFilter = !this.hideFilter;
        this.filterIco = this.variablesService.getFilterIco();
    }


    activateInfo() {
        this.activeTab = 2;
    }

    isCustomSearch() {
        this.customSearch = !this.customSearch;
    }


    updateRate() {
        this.auctionService.getTenderByAuctionId(this.auction.id).subscribe(
            (tenders)=> {
                this.tenderList = tenders;
                this.hasTender = this.tenderList.length > 0;
                this.tenderList.forEach((tender) => {
                    this.userService.getUserById(tender.userId).subscribe(
                        (user) => {
                            tender.user = user;
                        }
                    );
                });
            }
        );
    }
}

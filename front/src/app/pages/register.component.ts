import { Component, OnInit }        from '@angular/core';
import { SignUp } from "../interface/sign-up";
import {UserService} from "../service/user.service";
import {User} from "../model/user";
import {Router} from "@angular/router";

@Component({
    moduleId: module.id,
    selector: 'app-register',
    templateUrl: 'register.component.html',
    providers: [UserService]
})
export class RegisterComponent implements OnInit {
    public signUp: SignUp;
    service: UserService;

    isError: boolean;
    errorText: string;

    constructor(private userService:UserService, private router: Router) {
        this.service = userService;
    }

    ngOnInit() {
        this.signUp = {
            username: '',
            name: '',
            email: '',
            role: '',
            password: '',
            confirmPassword: ''
        }
    }

    save(model: SignUp, isValid: boolean) {
        if (isValid) {
            let user = new User();
            user.username = model.username;
            user.password = model.password;
            user.name = model.name;
            user.email = model.email;
            user.role = model.role;
            this.userService.createUser(user).subscribe(
                resp => {
                    this.isError = false;
                    this.userService.getAuth(resp.username, resp.password).subscribe(
                        (response) => {
                            localStorage.setItem('token', response.token);
                            localStorage.setItem('userId', response.user.id);
                            localStorage.setItem('role', response.user.role);
                            this.router.navigate(['/']);
                        }
                    );
                },
                err => {
                    this.isError = true;
                    this.errorText = err._body;
                }
            );
        }
    }

}

import { NgModule }                 from '@angular/core';
import { HttpModule } from '@angular/http';

import { p404Component }            from './404.component';
import { p500Component }            from './500.component';
import { LoginComponent }           from './login.component';
import { RegisterComponent }        from './register.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { PagesRoutingModule }       from './pages-routing.module';
import {EqualValidatorDirective} from "../directive/equal-validator.directive";

@NgModule({
    imports: [ PagesRoutingModule, ReactiveFormsModule, FormsModule, HttpModule],
    declarations: [
        p404Component,
        p500Component,
        LoginComponent,
        RegisterComponent,
        EqualValidatorDirective
    ]
})
export class PagesModule { }

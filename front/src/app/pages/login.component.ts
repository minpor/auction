import {Component} from "@angular/core";
import {UserService} from "../service/user.service";
import {SignIn} from "../interface/sign-in";
import {Router} from "@angular/router";

@Component({
    selector: 'app-login',
    templateUrl: 'login.component.html',
    providers: [UserService]
})
export class LoginComponent {
    public signIn: SignIn;
    service: UserService;
    
    constructor(private userService:UserService, private router: Router) {
        this.service = userService;
    }

    ngOnInit() {
        this.signIn = {
            username: '',
            password: ''
        }
    }

    login(model: SignIn, isValid: boolean) {
        if (isValid) {
            this.userService.getAuth(model.username, model.password).subscribe(
                (response) => {
                    localStorage.setItem('token', response.token);
                    localStorage.setItem('userId', response.user.id);
                    localStorage.setItem('role', response.user.role);
                    this.router.navigate(['/']);
                }
            );
        }
    }
}

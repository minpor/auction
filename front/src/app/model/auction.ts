export class Auction {
    id: number;
    auctionType: number;
    userId: number;
    locality: string;
    district: string;
    createdTime: string;
    days: number;
    budget: number;
    size: string;
    light: boolean;
}
import {Pagination} from "./pagination";
export class AuctionFilter {
    pagination: Pagination = new Pagination();
    input: any;
    id: number;
    userId: number;
    locality: string;
    district: string;
    dateFrom: string;
    dateTo: string;
    days: number;
    budget: number;
    size: string;
    light: boolean;

}
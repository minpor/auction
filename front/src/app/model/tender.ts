import {User} from "./user";
export class Tender {
    id: number;
    auctionId: number;
    createdTime: string;
    image: string;
    place: string;
    price: number;
    userId: number;
    user: User;
}
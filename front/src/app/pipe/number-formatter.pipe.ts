import {Pipe, PipeTransform} from "@angular/core";

@Pipe({
  name: 'numberFormatter',
  pure: true
})
export class NumberFormatterPipe implements PipeTransform {

  transform(value:any):any {
    if (!value) {
      return '';
    }
    return value.replace(new RegExp(',','g'), ' ');
  }

}

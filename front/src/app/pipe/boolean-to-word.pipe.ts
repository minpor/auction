import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'booleanToWord'
})
export class BooleanToWordPipe implements PipeTransform {

    transform(value: boolean): string {
        if (value == null) {
            return 'Не важно';
        }

        if (value) {
            return 'Да';
        } else if (!value) {
            return 'Нет';
        }
    }

}

import {Injectable} from "@angular/core";
import {Http, Headers, Response} from "@angular/http";
import {contentHeaders} from "../common/headers";
import {url} from "../common/urls";
import {Auction} from "../model/auction";
import {AuctionFilter} from "../model/auctionFilter";
import {Observable} from "rxjs/Observable";
import "rxjs/add/operator/map";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/toPromise";
import {Tender} from "../model/tender";

@Injectable()
export class AuctionService {

    auction:Auction = new Auction();
    auctionList:Auction[] = [];
    tender:Tender = new Tender();

    constructor(private http:Http) {
    }

    createAuthorizationHeader(headers:Headers) {
        headers.delete('Authorization');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('token'));
    }

    public auctionCreateService(auction:Auction):Observable<Auction> {
        let headers = contentHeaders;
        this.createAuthorizationHeader(headers);
        return this.http.post(url + "auctionCreate", auction, {headers: headers})
            .map((resp:Response)=> {
                let auctionResponse = resp.json();
                this.auction.id = auctionResponse.id;
                this.auction.locality = auctionResponse.locality;
                return this.auction;
            });
    }

    public auctionFilterService(auctionFilter:AuctionFilter){
        let headers = contentHeaders;
        this.createAuthorizationHeader(headers);
        return this.http.post(url + "auctionList", auctionFilter, {headers: headers})
            .map((responseData) => {
                return responseData.json();
            })
            .map((auctions:Array<any>) => {
                let result:Array<Auction> = [];
                if (auctions) {
                    auctions.forEach((auction) => {
                        let auctionObject:Auction = new Auction();
                        auctionObject.id = auction.id;
                        auctionObject.auctionType = auction.auctionType;
                        auctionObject.locality = auction.locality;
                        auctionObject.district = auction.district;
                        auctionObject.createdTime = auction.createdTime;
                        auctionObject.budget = auction.budget;
                        auctionObject.days = auction.days;
                        auctionObject.size = auction.size;
                        auctionObject.light = auction.light;
                        result.push(auctionObject);

                    });
                }
                return result;
            });
    }

    public auctionPagesCountFilterService(auctionFilter:AuctionFilter) {
        const headers = contentHeaders;
        this.createAuthorizationHeader(headers);
        return this.http.post(url + "/auction/filter/pages/count", auctionFilter, {headers: headers})
            .map((responseData) => {
                return +responseData.json();
            });
    }

    public createTenderService(tender:Tender):Observable<Tender> {
        const headers = contentHeaders;
        this.createAuthorizationHeader(headers);
        return this.http.post(url + "tender", tender, {headers: headers})
            .map((resp:Response) => {
                const tenderResponse = resp.json();
                this.tender.id = tenderResponse.id;
                this.tender.auctionId = tenderResponse.auctionId;
                this.tender.image = tenderResponse.image;
                this.tender.place = tenderResponse.place;
                this.tender.price = tenderResponse.price;
                this.tender.userId = tenderResponse.userId;
                return this.tender;
            });
    }
    
    public getTenderByAuctionId(auctionId:number) {
        const headers = contentHeaders;
        this.createAuthorizationHeader(headers);
        return this.http.get(url + "tender/" + auctionId, {headers: headers})
            .map((responseData) => {
                return responseData.json();
            })
            .map((tenders:Array<any>) => {
                let result:Array<Tender> = [];
                if (tenders) {
                    tenders.forEach((tender) => {
                        let tenderObject:Tender = new Tender();
                        tenderObject.id = tender.id;
                        tenderObject.auctionId = tender.auctionId;
                        tenderObject.image = tender.image;
                        tenderObject.createdTime = tender.createdTime;
                        tenderObject.place = tender.place;
                        tenderObject.price = tender.price;
                        tenderObject.userId = tender.userId;
                        result.push(tenderObject);
                    });
                }
                return result;
            });
        
    }
}
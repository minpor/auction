import {Injectable} from "@angular/core";
import {Http, Headers, Request, RequestMethod, RequestOptions, URLSearchParams, Response} from "@angular/http";
import "rxjs/add/operator/map";
import "rxjs/add/operator/toPromise";
import 'rxjs/add/operator/catch';
import { contentHeaders } from '../common/headers';
import  { User} from "../model/user";
import {Observable} from "rxjs/Observable";
import {url} from "../common/urls";


@Injectable()
export class UserService {

    user:User = new User();

    constructor(private http: Http) {}

    createAuthorizationHeader(headers: Headers) {
        if (!headers.has('Authorization')) {
            headers.append('Authorization', 'Bearer ' + localStorage.getItem('token'));
        }
    }

    public getAuth(username: string, password: string) {
        let params = new URLSearchParams();
        params.set('username', username);
        params.set('password', password);
        let body = params.toString();
        return this.http.post(url + "authenticate?"+ params, '')
            .map((response) => {
                return response.json();
            });
    }

    public createUser(user: User) {
        return this.http.post(url + "register", user, {headers: contentHeaders})
            .map((responseData) => {
                return responseData.json();
            });
    }

    public getAllUsers() {
        let headers = new Headers();
        this.createAuthorizationHeader(headers);
        return this.http.get(url + "api/users", {headers: headers})
            .subscribe(
                response => {
                },
                error => {
                    alert(error.text());
                }
            );
    }
    
    public getUserById(id:number):Observable<User> {
        const headers = contentHeaders;
        this.createAuthorizationHeader(headers);
        return this.http.get(url + "api/user/" + id, {headers: headers})
            .map((resp:Response) => {
                const userResponse = resp.json();
                this.user.id = userResponse.id;
                this.user.name = userResponse.name;
                this.user.username = userResponse.username;
                this.user.password = userResponse.password;
                this.user.role = userResponse.role;
                return this.user;
            });
    }

    private handleError(error: any){
        return Promise.reject(error.message || error);
    }
}

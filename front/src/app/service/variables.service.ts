import { Injectable } from '@angular/core';

@Injectable()
export class VariablesService {

    private _filterIco:string="icon-arrow-up";

    constructor() {

    }

    getFilterIco():string {
        if (this._filterIco  == 'icon-arrow-up') {
            this.setFilterIco('icon-arrow-down');
        } else if (this._filterIco != 'icon-arrow-up') {
            this.setFilterIco('icon-arrow-up');
        }
        return this._filterIco;
    }


    setFilterIco(value:string) {
        this._filterIco = value;
    }
}

import { NgModule }      from '@angular/core';
import { CommonModule }  from '@angular/common';
import { AccountRouting } from './account.routing';
import { AccountComponent } from './account.component';

@NgModule({
    imports: [
        CommonModule,
        AccountRouting
    ],
    providers: [],
    declarations: [
        AccountComponent
    ],
    exports: []
})
export class AccountModule {

}

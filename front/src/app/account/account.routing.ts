import { ModuleWithProviders }  from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AccountComponent } from './account.component';

const routes: Routes = [
    {   path: '', 
        component: AccountComponent, 
        data: {
            title: 'Лицевой счет'
        }
    },
];

export const AccountRouting: ModuleWithProviders = RouterModule.forChild(routes);

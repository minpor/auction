import {Component, DoCheck, OnInit} from "@angular/core";
import {UserUtils} from "../common/user-utils";
import {FileUploader, FileUploaderOptions} from 'ng2-file-upload';
import { avatarUrl } from '../common/avatar';

export class Headers {
    name: string;
    value: string;

    constructor(name: string, value: string) {
        this.name = name;
        this.value = value;
    }
}
const url = avatarUrl;
const headers:Array<Headers> = [//new Headers('Accept','application/json'),
    //new Headers('Content-Type','multipart/form-data'),
    new Headers('Authorization', 'Bearer ' + localStorage.getItem('token'))];

@Component({
    templateUrl: 'main.component.html'

})
export class MainComponent implements OnInit, DoCheck {
    userUtils:UserUtils;


    constructor( ) { }

    ngOnInit(): void {

    }

    ngDoCheck() {
        this.userUtils = new UserUtils();
    }

    public uploader:FileUploader = this.fileUploader();

    private fileUploader() {
        this.uploader
        return new FileUploader({url, headers});
    }
    public hasBaseDropZoneOver:boolean = false;
    public hasAnotherDropZoneOver:boolean = false;

    public fileOverBase(e:any):void {
        this.hasBaseDropZoneOver = e;
    }

    public fileOverAnother(e:any):void {
        this.hasAnotherDropZoneOver = e;
    }
}

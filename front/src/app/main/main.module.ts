import { NgModule }                 from '@angular/core';
import { ChartsModule }             from 'ng2-charts/ng2-charts';

import { MainComponent }       from './main.component';
import { MainRoutingModule }   from './main-routing.module';
import { DropdownModule } from "ng2-bootstrap/dropdown";
import {FileUploadModule} from "ng2-file-upload";
import {CommonModule} from "@angular/common";

@NgModule({
    imports: [
        MainRoutingModule,
        ChartsModule,
        DropdownModule,
        FileUploadModule,
        CommonModule
    ],
    declarations: [
        MainComponent
    ]
})
export class MainModule { }

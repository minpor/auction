import {Component, OnInit} from "@angular/core";
import {UserService} from "../service/user.service";
import {UserUtils} from "../common/user-utils";

@Component({
    selector: 'app-dashboard',
    templateUrl: './full-layout.component.html',
    providers: [UserService]
})
export class FullLayoutComponent implements OnInit {

    service: UserService;
    username: string;

    userUtils:UserUtils = new UserUtils();
    
    constructor(private userService:UserService) {
        this.service = userService;
    }

    public disabled:boolean = false;
    public status:{isopen:boolean} = {isopen: false};

    public toggled(open:boolean):void {
        console.log('Dropdown is now: ', open);
    }

    public toggleDropdown($event:MouseEvent):void {
        $event.preventDefault();
        $event.stopPropagation();
        this.status.isopen = !this.status.isopen;
    }

    ngOnInit(): void {
        const userId = localStorage.getItem('userId');
        if (userId!=null) {
            this.userService.getUserById(+userId).subscribe((resp) => {
                this.username = resp.username;
            });
        }
    }

    logout() {
        localStorage.removeItem('userId');
        localStorage.removeItem('token');
        localStorage.removeItem('role');
        this.userUtils.userRole = null;
        this.username = null;
    }
}

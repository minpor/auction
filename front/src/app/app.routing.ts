import { NgModule }                 from '@angular/core';
import { Routes,
         RouterModule }             from '@angular/router';

//Layouts
import { FullLayoutComponent }      from './layouts/full-layout.component';
import { SimpleLayoutComponent }    from './layouts/simple-layout.component';

export const routes: Routes = [
    {
        path: '',
        redirectTo: 'main',
        pathMatch: 'full',
    },
    {
        path: '',
        component: FullLayoutComponent,
        data: {
            title: 'Главная страница'
        },
        children: [
            {
                path: 'main',
                loadChildren: './main/main.module#MainModule'
            },
            {
                path: 'stat',
                loadChildren: './stat/stat.module#StatModule'
            },
            {
                path: 'auction',
                loadChildren: './auction/auction.module#AuctionModule'
            },
            {
                path: 'account',
                loadChildren: './account/account.module#AccountModule'
            }
        ]
    },
    {
        path: 'pages',
        component: SimpleLayoutComponent,
        data: {
            title: 'Pages'
        },
        children: [
            {
                path: '',
                loadChildren: './pages/pages.module#PagesModule',
            }
        ]
    },
    {
        path: '**',
        redirectTo: '/pages/404'
    }
];

@NgModule({
    imports: [ RouterModule.forRoot(routes) ],
    exports: [ RouterModule ]
})
export class AppRoutingModule {}

import {url} from "./urls";

export const avatarUrl: string = url.concat('contractor/avatar');
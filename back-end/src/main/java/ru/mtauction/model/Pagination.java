package ru.mtauction.model;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class Pagination {
	private int limit;
	private int page;
}

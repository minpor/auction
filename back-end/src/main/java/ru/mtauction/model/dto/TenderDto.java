package ru.mtauction.model.dto;

import lombok.Getter;
import lombok.Setter;
import ru.mtauction.domain.Tender;

import java.math.BigDecimal;

@Getter
@Setter
public class TenderDto {
	private long id;
	private long auctionId;
	private Integer userId;
	private BigDecimal price;
	private String place;
	private String image;
	private java.sql.Timestamp createdTime;

	public TenderDto(Tender tender) {
		this.id = tender.getId();
		this.auctionId = tender.getAuctionId();
		this.userId = tender.getUserId();
		this.price = tender.getPrice();
		this.place = tender.getPlace();
		this.image = tender.getImage();
		this.createdTime = tender.getCreatedTime();
	}
}

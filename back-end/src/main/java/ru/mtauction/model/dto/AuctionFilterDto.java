package ru.mtauction.model.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import ru.mtauction.model.Pagination;

import java.sql.Timestamp;

@Getter
@Setter
@ToString
public class AuctionFilterDto {
	private Pagination pagination;
	private Object input;
	private Integer id;
	private String locality;
	private String district;
	private Timestamp dateFrom;
	private Timestamp dateTo;
	private Status status;
	private String size;
	private Boolean light;
}

package ru.mtauction.model.dto;

public enum Status {
	ACTIVE,
	CLOSED,
	REOPEN,
	BANNED
}

package ru.mtauction.model.dto;

import lombok.Getter;
import lombok.Setter;
import ru.mtauction.domain.Auction;

import java.util.Set;
import java.util.stream.Collectors;

@Getter
@Setter
public class AuctionDto {
	private int id;
	private Short auctionType;
	private long userId;
	private String locality;
	private String district;
	private java.sql.Timestamp createdTime;
	private int days;
	private String size;
	private Boolean light;
	private Set<TenderDto> tenders;

	public AuctionDto(Auction auction) {
		this.id = auction.getId();
		this.auctionType = auction.getAuctionType();
		this.userId = auction.getUserId();
		this.locality = auction.getLocality();
		this.district = auction.getDistrict();
		this.createdTime = auction.getCreatedTime();
		this.days = auction.getDays();
		this.size = auction.getSize();
		this.light = auction.getLight();
		this.tenders = auction.getTenders().stream().map(TenderDto::new).collect(Collectors.toSet());
	}


}

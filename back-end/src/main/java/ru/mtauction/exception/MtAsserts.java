package ru.mtauction.exception;

public class MtAsserts {

	public MtAsserts() {
		throw new UnsupportedOperationException("Not instantiable");
	}

	public static void badRequestException(String message) throws BadRequestException {
		throw new BadRequestException(message);
	}

	public static void forbiddenException(String message) throws ForbiddenException {
		throw new ForbiddenException(message);
	}

	public static void internalErrorException(String message) throws InternalErrorException {
		throw new InternalErrorException(message);
	}

	public static void notFoundException(String message) throws NotFoundException {
		throw new NotFoundException(message);
	}

	public static void unauthorizedException(String message) throws UnauthorizedException {
		throw new UnauthorizedException(message);
	}
}

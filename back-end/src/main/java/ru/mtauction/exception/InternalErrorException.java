package ru.mtauction.exception;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class InternalErrorException extends RuntimeException {

	private static final long serialVersionUID = -5300763817450262030L;

	public InternalErrorException() {
	}

	public InternalErrorException(String message) {
		super(message);
	}
}

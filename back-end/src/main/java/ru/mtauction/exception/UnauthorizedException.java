package ru.mtauction.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.UNAUTHORIZED)
public class UnauthorizedException extends RuntimeException {

	private static final long serialVersionUID = -4708611933823523652L;

	public UnauthorizedException() {
	}

	public UnauthorizedException(String message) {
		super(message);
	}
}

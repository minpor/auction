package ru.mtauction.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.FORBIDDEN)
public class ForbiddenException extends RuntimeException {

	private static final long serialVersionUID = -7300476754302463002L;

	public ForbiddenException() {
	}

	public ForbiddenException(String message) {
		super(message);
	}
}

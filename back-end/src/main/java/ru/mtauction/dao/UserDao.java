package ru.mtauction.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.mtauction.domain.User;


public interface UserDao extends JpaRepository<User, Long> {
	User findOneByUsername(String username);
	User findOneById(Integer id);

}

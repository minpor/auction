package ru.mtauction.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import ru.mtauction.domain.Auction;

import java.util.List;


public interface AuctionDao extends JpaRepository<Auction, Long>, JpaSpecificationExecutor<Auction> {
	@Override
	List<Auction> findAll();
	Auction findOneById(Integer id);
}

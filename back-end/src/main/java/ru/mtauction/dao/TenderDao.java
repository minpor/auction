package ru.mtauction.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import ru.mtauction.domain.Tender;

import java.util.List;

public interface TenderDao extends JpaRepository<Tender, Long>, JpaSpecificationExecutor<Tender> {
	@Override
	List<Tender> findAll();
}

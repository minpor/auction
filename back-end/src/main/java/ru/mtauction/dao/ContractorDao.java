package ru.mtauction.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.mtauction.domain.Contractor;
import ru.mtauction.domain.User;

public interface ContractorDao extends JpaRepository<Contractor, Long> {
	Contractor findByUser(User user);

}

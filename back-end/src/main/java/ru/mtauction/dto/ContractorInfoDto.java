package ru.mtauction.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ContractorInfoDto {
	private String photo;
	private String username;
	private PersonalDto personal;
}

package ru.mtauction.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AvatarDto {
	private int userId;
	private byte[] avatar;
	private String fileName;

	public AvatarDto(int userId, byte[] avatar, String fileName) {
		this.userId = userId;
		this.avatar = avatar;
		this.fileName = fileName;
	}

	public AvatarDto(){}
}

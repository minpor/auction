package ru.mtauction.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;

@Getter
@Setter
@ToString
public class BalanceInfoDto {
	/** Поступление денежных средств */
	private BigDecimal moneyIn;
	/** Трата денежных средств */
	private BigDecimal moneyOut;
	/** Текуший баланс */
	private BigDecimal balance;
}

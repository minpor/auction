package ru.mtauction.dto;

import io.jsonwebtoken.lang.Assert;
import lombok.Getter;
import ru.mtauction.domain.User;
import ru.mtauction.domain.enums.Role;

@Getter
public class UserDto {
	private int id;
	private String name;
	private String username;
	private String email;
	private Role role;

	public UserDto(User user) {
		Assert.notNull(user);
		this.id = user.getId();
		this.name = user.getName();
		this.username = user.getUsername();
		this.email = user.getEmail();
		this.role = user.getRole();
	}

	public UserDto(int id, String name, String username, String email, String role) {
		this.id = id;
		this.name = name;
		this.username = username;
		this.email = email;
		this.role = Role.valueOf(role);
	}
}

package ru.mtauction.util;

public class ConverterUtils {

	public Long getLongOrNull(Object input) {
		if (input instanceof Long) {
			return Long.parseLong(input.toString());
		} else {
			return null;
		}
	}

	public Integer getIntegerOrNull(Object input) {
		if (input instanceof Integer) {
			return Integer.parseInt(input.toString());
		} else {
			return null;
		}
	}

	public String getStringOrNull(Object input) {
		if (input instanceof String) {
			return input.toString();
		} else {
			return null;
		}
	}

	public Boolean getBoolean(Object input) {
		if (input instanceof Boolean) {
			return (Boolean) input;
		} else {
			return null;
		}
	}
}

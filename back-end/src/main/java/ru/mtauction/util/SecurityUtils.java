package ru.mtauction.util;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import ru.mtauction.dto.UserDto;

import java.util.Optional;

public class SecurityUtils {
	private SecurityUtils() {
		throw new UnsupportedOperationException("Not instantiable");
	}

	public static int getCurrentUserId() {
		return getCurrentUser().getId();
	}

	private static UserDto getCurrentUser() {
		return Optional.ofNullable(SecurityContextHolder.getContext().getAuthentication())
				.map(Authentication::getPrincipal)
				.filter(o1 -> o1 instanceof UserDto)
				.map(p -> (UserDto) p)
				.orElseThrow(() -> new IllegalStateException("No authorization"));
	}



}

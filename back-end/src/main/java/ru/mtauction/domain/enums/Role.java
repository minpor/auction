package ru.mtauction.domain.enums;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Collection;
import java.util.HashSet;

public enum Role {

	ADMIN,
	CONTRACTOR,
	CUSTOMER;

	public String toRole() {
		return "ROLE_".concat(this.toString());
	}

	public GrantedAuthority toAuthority() {
		return new SimpleGrantedAuthority(this.toRole());
	}

	public Collection<? extends GrantedAuthority> toAuthorities() {

		Collection<GrantedAuthority> authorities = new HashSet<>();

		for (Role role : Role.values()) {
			if (this.toString().equals(Role.ADMIN.toString())) {
				authorities.add(role.toAuthority());
			} else {
				if (this.toString().equals(role.toString())) {
					authorities.add(this.toAuthority());
				}
			}
		}

		return authorities;
	}

}

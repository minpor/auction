package ru.mtauction.domain.enums;

public enum ContractorType {
	/** Физическое лицо */
	INDIVIDUAL,
	/** Юридическое лицо */
	LEGAL
}

package ru.mtauction.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Embeddable;

@Getter
@Setter
@Embeddable
public class Company {
	private String inn;
	private String ogrn;
	private String kpp;
	private String name;
}

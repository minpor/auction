package ru.mtauction.domain;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name="auction")
public class Auction implements Serializable {
	private static final long serialVersionUID = -8365660537310802052L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", length = 6, nullable = false)
	private int id;
	@Column(name = "auction_type")
	private Short auctionType;
	@Column(name = "user_id", columnDefinition = "int8")
	private long userId;
	private String locality;
	private String district;
	@Column(name = "created_time", columnDefinition = "timestamp default now()", insertable = false)
	@Type(type = "timestamp")
	private java.sql.Timestamp createdTime;
	private int days;
	private BigDecimal budget;
	private String size;
	private Boolean light;
	@OneToMany(mappedBy = "auctionId")
	private Set<Tender> tenders;
}

package ru.mtauction.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import ru.mtauction.domain.enums.Role;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name="\"user\"")
public class User implements UserDetails, Serializable {
	private static final long serialVersionUID = 5213800118899435734L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", length = 6, nullable = false, insertable = false, updatable = false)
	private int id;
	@Column
	private String name;
	@Column(unique = true)
	private String username;
	@Column
	private String email;
	@Column
	private String password;
	@Column(nullable = false)
	@Enumerated(EnumType.STRING)
	private Role role;
	@OneToMany(mappedBy = "userId")
	private Set<Auction> auctions;
	@OneToMany(mappedBy = "userId")
	private Set<Tender> tenders;

	@OneToOne
	@PrimaryKeyJoinColumn
	private Contractor contractor;


	@JsonIgnore
	@Override
	public boolean isEnabled() {
		return true;
	}

	@JsonIgnore
	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@JsonIgnore
	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@JsonIgnore
	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@JsonIgnore
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return this.getRole().toAuthorities();
	}

	@Override
	public String getPassword() {
		return password;
	}

	@Override
	public String getUsername() {
		return username;
	}


}

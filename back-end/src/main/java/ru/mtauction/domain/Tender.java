package ru.mtauction.domain;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Getter
@Setter
@Entity
@Table(name="tender")
public class Tender implements Serializable {
	private static final long serialVersionUID = 4163958210361576899L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", length = 6, nullable = false)
	private long id;
	@Column(name = "auction_id")
	private long auctionId;
	@Column(name = "user_id")
	private Integer userId;
	@Column(name = "price")
	private BigDecimal price;
	@Column(name = "place")
	private String place;
	private String image;
	@Column(name = "created_time", columnDefinition = "timestamp default now()", insertable=false)
	@Type(type="timestamp")
	private java.sql.Timestamp createdTime;
}

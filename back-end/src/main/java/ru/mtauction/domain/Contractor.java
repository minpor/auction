package ru.mtauction.domain;

import lombok.Getter;
import lombok.Setter;
import ru.mtauction.domain.enums.ContractorType;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@Entity
@Table(name = "contractor")
public class Contractor implements Serializable {
	private static final long serialVersionUID = -4322355188732902479L;

	@Id
	@Column(name = "id", length = 6, nullable = false, insertable = false, updatable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Enumerated(EnumType.STRING)
	@Column(name = "contractor_type")
	private ContractorType contractorType;
	@Embedded
	private Company company;
	@Embedded
	private Personal personal;
	private String avatar;
	@OneToOne(fetch = FetchType.LAZY)
	private User user;


}

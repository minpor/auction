package ru.mtauction.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import ru.mtauction.domain.Auction;
import ru.mtauction.model.dto.AuctionDto;
import ru.mtauction.model.dto.AuctionFilterDto;
import ru.mtauction.service.AuctionService;

import java.util.List;
import java.util.Set;

@CrossOrigin(maxAge = 3600)
@RestController
public class AuctionRestController {
	@Autowired
	private AuctionService auctionService;

	/** Создание нового аукциона */
	@Secured({"ROLE_CUSTOMER", "ROLE_ADMIN"})
	@RequestMapping(value = "/auctionCreate", method = RequestMethod.POST)
	public ResponseEntity<Auction> auctionCreate(@RequestBody Auction auction) {
		return auctionService.saveAuction(auction);
	}

	/** Получение списка аукционов по фильтру */
	@RequestMapping(value = "/auctionList", method = RequestMethod.POST)
	public List<AuctionDto> auctionsByFilter(@RequestBody AuctionFilterDto auctionFilterDto) {
		return auctionService.getAuctionListByFilter(auctionFilterDto);
	}

	/** Количество элементов в запросе */
	@RequestMapping(value = "/auction/filter/pages/count", method = RequestMethod.POST)
	public int auctionPagesCountByFilter(@RequestBody AuctionFilterDto auctionFilterDto) {
		return auctionService.getAuctionPagesCountByFilter(auctionFilterDto);
	}


}

package ru.mtauction.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ru.mtauction.dto.AvatarDto;
import ru.mtauction.service.ContractorService;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping(value = "/contractor")
public class ContractorAccountController {

	@Autowired
	private ContractorService contractorService;

	/** Получить аватар */
	@Secured({"ROLE_CONTRACTOR"})
	@RequestMapping(value = "avatar", method = RequestMethod.GET)
	public AvatarDto getAvatar(@RequestParam int userId) {
		return contractorService.getAvatar(userId);
	}


	/** Создать аватар */
	@Secured({"ROLE_CONTRACTOR"})
	@RequestMapping(value = "avatar", method = RequestMethod.POST)
	public void createAvatar(@RequestParam("file") MultipartFile file)  {
		contractorService.createAvatar(file);
	}
}
package ru.mtauction.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import ru.mtauction.domain.Tender;
import ru.mtauction.model.dto.TenderDto;
import ru.mtauction.service.TenderService;

import java.util.List;

@CrossOrigin(maxAge = 3600)
@RestController
public class TenderRestController {

	@Autowired
	private TenderService tenderService;


	/** Сделать ставку */
	@RequestMapping(value = "/tender", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
	public Tender createTender(@RequestBody Tender tender) {
		return tenderService.createTender(tender);
	}

	/** Получить список торгов(ставок) по аукциону */
	@RequestMapping(value = "/tender/{auctionId}", method = RequestMethod.GET)
	public List<TenderDto> getTenderByAuctionId(@PathVariable long auctionId) {
		return tenderService.getTenderByAuctionId(auctionId);
	}
}

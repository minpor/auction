package ru.mtauction.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.mtauction.domain.User;
import ru.mtauction.service.UserService;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.Principal;
import java.util.List;
import java.util.Map;

@CrossOrigin(maxAge = 3600)
@RestController
public class UserRestController {
	@Autowired
	private UserService userService;

	@RequestMapping(value = "/api/users", method = RequestMethod.GET)
	public List<User> users() {
		return userService.findAll();
	}

	@RequestMapping(value = "/api/user/{id}", method = RequestMethod.GET)
	public ResponseEntity<User> userById(@PathVariable Integer id) {
		return userService.findByUserId(id);
	}

	/** Регистрация пользователя */
	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public ResponseEntity<?> createUser(@RequestBody User user) {
		return userService.createUser(user);
	}

	/** Получить пользователя */
	@RequestMapping("/user")
	public User user(Principal principal) {
		return userService.getUserByPrincipal(principal);
	}

	/** Авториация по логину и паролю */
	@RequestMapping(value = "/authenticate", method = RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> login(@RequestParam String username, @RequestParam String password,
	                                                 HttpServletResponse response) throws IOException {
		return userService.getLogin(username, password);
	}
}

package ru.mtauction.service;

import ru.mtauction.domain.Tender;
import ru.mtauction.model.dto.TenderDto;

import javax.transaction.Transactional;
import java.util.List;

public interface TenderService {

	@Transactional
	Tender createTender(Tender tender);

	@Transactional
	List<TenderDto> getTenderByAuctionId(long auctionId);
}

package ru.mtauction.service.impl;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import ru.mtauction.dao.ContractorDao;
import ru.mtauction.dao.UserDao;
import ru.mtauction.domain.Contractor;
import ru.mtauction.domain.User;
import ru.mtauction.dto.*;
import ru.mtauction.exception.MtAsserts;
import ru.mtauction.service.ContractorService;
import ru.mtauction.util.SecurityUtils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import static org.hibernate.validator.internal.util.Contracts.assertNotNull;

@Service
public class ContractorServiceImpl implements ContractorService {

	@Autowired
	private UserDao userDao;
	@Autowired
	private ContractorDao contractorDao;

	@Value("${path.files}")
	private String path;


	@Override
	public AvatarDto getAvatar(int userId) {
		User user = userDao.findOneById(userId);
		assertNotNull(user, "user is null");

		Contractor contractor = contractorDao.findByUser(user);
		assertNotNull(contractor, "contractor is null");

		String filePath = new StringBuilder()
				.append(path)
				.append("avatar/")
				.append(contractor.getAvatar())
				.toString();

		byte[] bytes = null;
		try {
			File file = new File(filePath);
			int length = (int) file.length();
			BufferedInputStream reader = new BufferedInputStream(new FileInputStream(file));
			bytes = new byte[length];
			reader.read(bytes, 0, length);
			reader.close();
		} catch (IOException e) {
			MtAsserts.badRequestException(e.getMessage());
		}

		return new AvatarDto(userId, bytes, contractor.getAvatar());
	}

	@Override
	public void createAvatar(MultipartFile file) {
		User user = userDao.findOneById(SecurityUtils.getCurrentUserId());
		assertNotNull(user, "user is null");

		Contractor contractorByUser = contractorDao.findByUser(user);

		Contractor contractor = new Contractor();
		if (contractorByUser == null) {
			contractor.setUser(user);

		} else if (contractorByUser.getAvatar() != null) {
			MtAsserts.badRequestException("Аватар уже создан");
		}

		try {
			String filePath = new StringBuilder()
					.append(path)
					.append("avatar/")
					.append(file.getOriginalFilename())
					.toString();

			FileUtils.writeByteArrayToFile(new File(filePath), file.getBytes());
			contractor.setAvatar(file.getOriginalFilename());

			contractorDao.save(contractor);
		} catch (IOException e) {
			MtAsserts.badRequestException(e.getMessage());
		}


	}

	@Override
	public void updateAvatar(AvatarDto dto) {

	}

	@Override
	public PersonalDto getPersonalInfo(int userId) {
		return null;
	}

	@Override
	public void updatePersonalInfo(PersonalDto dto) {

	}

	@Override
	public ContractorCompanyInfoDto getCompanyInfo(int userId) {
		return null;
	}

	@Override
	public void updateCompanyInfo(ContractorCompanyInfoDto dto) {

	}

	@Override
	public TenderStatDto getTenderByUserStat(int userId) {
		return null;
	}

	@Override
	public BalanceInfoDto getBalanceInfo(int userId) {
		return null;
	}

	private void createContractorByUser(User user) {

	}
}

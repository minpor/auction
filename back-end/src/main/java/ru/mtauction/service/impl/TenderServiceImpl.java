package ru.mtauction.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import ru.mtauction.dao.TenderDao;
import ru.mtauction.domain.Tender;
import ru.mtauction.model.dto.TenderDto;
import ru.mtauction.service.TenderService;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TenderServiceImpl implements TenderService {

	@Autowired
	private TenderDao tenderDao;

	@Override
	public Tender createTender(Tender tender) {
		return tenderDao.save(tender);
	}

	@Override
	public List<TenderDto> getTenderByAuctionId(long auctionId) {
		Specification<Tender> specification = (root, query, cb) -> cb.equal(root.get("auctionId"), auctionId);
		Specifications<Tender> where = Specifications.where(specification);
		List<Tender> tenderList = tenderDao.findAll(where);

		return tenderList
				.stream()
				.map(TenderDto::new)
				.collect(Collectors.toList());
	}
}

package ru.mtauction.service.impl;

import io.jsonwebtoken.lang.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import ru.mtauction.dao.AuctionDao;
import ru.mtauction.domain.Auction;
import ru.mtauction.model.Pagination;
import ru.mtauction.model.dto.AuctionDto;
import ru.mtauction.model.dto.AuctionFilterDto;
import ru.mtauction.service.AuctionService;
import ru.mtauction.util.ConverterUtils;

import javax.persistence.criteria.Predicate;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class AuctionServiceImpl implements AuctionService {

	@Autowired
	private AuctionDao auctionDao;

	@Override
	public ResponseEntity<Auction> saveAuction(Auction auction) {
		return new ResponseEntity<>(auctionDao.save(auction), HttpStatus.OK);
	}

	@Override
	public List<AuctionDto> getAuctionListByFilter(AuctionFilterDto dto) {
		return getAuctionDto(getAuctionPage(dto).getContent());
	}

	@Override
	public int getAuctionPagesCountByFilter(AuctionFilterDto dto) {
		return (int) getAuctionPage(dto).getTotalElements();
	}

	private Page<Auction> getAuctionPage(AuctionFilterDto dto) {
		Pagination pagination = dto.getPagination();
		Assert.notNull(pagination, "pagination is null");

		PageRequest pageable = new PageRequest(pagination.getPage() - 1, pagination.getLimit());
		Specifications<Auction> where = Specifications.where(fillConditional(dto));

		return auctionDao.findAll(where, pageable);
	}


	private List<AuctionDto> getAuctionDto(List<Auction> auctionList) {
		return auctionList
				.stream()
				.map(AuctionDto::new)
				.collect(Collectors.toList());
	}

	private Specification<Auction> fillConditional(AuctionFilterDto dto) {
		return (root, query, cb) -> {

			final List<Predicate> predicates = new ArrayList<>();

			Object input = dto.getInput();
			Integer id = dto.getId();
			String locality = dto.getLocality();
			String district = dto.getDistrict();
			Boolean light = dto.getLight();
			Timestamp dateTo = dto.getDateTo();
			Timestamp dateFrom = dto.getDateFrom();
			String size = dto.getSize();

			boolean empty = StringUtils.isEmpty(input);
			if (!empty) {
				ConverterUtils converterUtils = new ConverterUtils();
				if (converterUtils.getIntegerOrNull(input) != null) {
					id = (Integer) input;
				} else {
					locality = input.toString();
					district = input.toString();
					light = converterUtils.getBoolean(input);
					size = input.toString();
				}
			}

			if (id != null) {
				predicates.add(cb.equal(root.get("id"), id));
			}
			if (!StringUtils.isEmpty(locality)) {
				predicates.add(cb.equal(root.get("locality"), locality));
			}
			if (!StringUtils.isEmpty(district)) {
				predicates.add(cb.equal(root.get("district"), district));
			}
			if (light != null) {
				predicates.add(cb.equal(root.get("light"), light));
			}
			if (dateFrom != null && dateTo != null) {
				predicates.add(cb.between(root.get("createdTime"), dateFrom, dateTo));
			} else if (dateFrom != null) {
				predicates.add(cb.greaterThan(root.get("createdTime"), dateFrom));
			} else if (dateTo != null) {
				predicates.add(cb.lessThan(root.get("createdTime"), dateTo));
			}

			if (!StringUtils.isEmpty(size)) {
				predicates.add(cb.equal(root.get("size"), size));
			}

			Predicate[] restrictions = predicates.toArray(new Predicate[predicates.size()]);

			if (!empty) {
				return cb.or(restrictions);
			} else {
				return cb.and(restrictions);
			}
		};
	}


}

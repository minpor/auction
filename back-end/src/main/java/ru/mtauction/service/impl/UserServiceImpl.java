package ru.mtauction.service.impl;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import ru.mtauction.dao.UserDao;
import ru.mtauction.domain.User;
import ru.mtauction.dto.UserDto;
import ru.mtauction.service.UserService;
import ru.mtauction.util.SecurityUtils;

import java.security.Principal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDao userDao;

	@Override
	public List<User> findAll() {
		return userDao.findAll();
	}

	@Override
	public ResponseEntity<User> findByUserId(int userId) {
		User user = userDao.findOneById(userId);
		if (user == null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<>(user, HttpStatus.OK);
		}
	}

	@Override
	public ResponseEntity<?> createUser(User user) {
		if (userDao.findOneByUsername(user.getUsername()) != null) {
			return new ResponseEntity<>("Пользователь уже существует", HttpStatus.CONFLICT);
		}
		return new ResponseEntity<>(userDao.save(user), HttpStatus.CREATED);
	}

	@Override
	public User getUserByPrincipal(Principal principal) {
		return userDao.findOneById(SecurityUtils.getCurrentUserId());
	}

	@Override
	public ResponseEntity<Map<String, Object>> getLogin(String username, String password) {
		User user = userDao.findOneByUsername(username);
		Map<String, Object> tokenMap = new HashMap<>();
		if (user != null && user.getPassword().equals(password)) {
			UserDto userDto = new UserDto(user);
			String token = Jwts.builder()
					.setSubject(username)
					.claim("user", userDto)
					.setIssuedAt(new Date())
					.signWith(SignatureAlgorithm.HS256, "secretkey")
					.compact();
			tokenMap.put("token", token);
			tokenMap.put("user", userDto);
			return new ResponseEntity<>(tokenMap, HttpStatus.OK);
		} else {
			tokenMap.put("token", null);
			return new ResponseEntity<>(tokenMap, HttpStatus.UNAUTHORIZED);
		}
	}
}

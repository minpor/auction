package ru.mtauction.service;

import org.springframework.http.ResponseEntity;
import ru.mtauction.domain.Auction;
import ru.mtauction.model.dto.AuctionDto;
import ru.mtauction.model.dto.AuctionFilterDto;

import javax.transaction.Transactional;
import java.util.List;


public interface AuctionService {

	@Transactional
	ResponseEntity<Auction> saveAuction(Auction auction);

	@Transactional
	List<AuctionDto> getAuctionListByFilter(AuctionFilterDto auctionFilterDto);

	@Transactional
	int getAuctionPagesCountByFilter(AuctionFilterDto auctionFilterDto);

}

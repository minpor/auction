package ru.mtauction.service;

import org.springframework.http.ResponseEntity;
import ru.mtauction.domain.User;

import javax.transaction.Transactional;
import java.security.Principal;
import java.util.List;
import java.util.Map;

public interface UserService {

	@Transactional
	List<User> findAll();

	@Transactional
	ResponseEntity<User> findByUserId(int userId);

	@Transactional
	ResponseEntity<?> createUser(User user);

	@Transactional
	User getUserByPrincipal(Principal principal);

	@Transactional
	ResponseEntity<Map<String, Object>> getLogin(String username, String password);

}

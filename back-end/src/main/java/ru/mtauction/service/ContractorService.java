package ru.mtauction.service;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import ru.mtauction.dto.*;


public interface ContractorService {
	@Transactional(readOnly = true)
	AvatarDto getAvatar(int userId);

	@Transactional
	void createAvatar(MultipartFile file);

	@Transactional
	void updateAvatar(AvatarDto dto);

	@Transactional(readOnly = true)
	PersonalDto getPersonalInfo(int userId);

	@Transactional
	void updatePersonalInfo(PersonalDto dto);

	@Transactional
	ContractorCompanyInfoDto getCompanyInfo(int userId);

	@Transactional
	void updateCompanyInfo(ContractorCompanyInfoDto dto);

	@Transactional
	TenderStatDto getTenderByUserStat(int userId);

	@Transactional
	BalanceInfoDto getBalanceInfo(int userId);
}
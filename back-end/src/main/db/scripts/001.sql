-- Table: public."user"

-- DROP TABLE public."user";

CREATE TABLE public."user"
(
  id serial,
  email character varying(255),
  name character varying(255),
  password character varying(255),
  role character varying(255) NOT NULL,
  username character varying(255),
  CONSTRAINT user_pkey PRIMARY KEY (id),
  CONSTRAINT uk_sb8bbouer5wak8vyiiy4pf2bx UNIQUE (username)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public."user" OWNER TO postgres;


-- Table: public.auction

-- DROP TABLE public.auction;

CREATE TABLE public.auction
(
  id serial,
  auction_type smallint,
  budget numeric(19,2),
  created_time timestamp without time zone DEFAULT now(),
  days integer NOT NULL,
  district character varying(255),
  light boolean,
  locality character varying(255),
  size character varying(255),
  user_id bigint,
  CONSTRAINT auction_pkey PRIMARY KEY (id),
  CONSTRAINT fk5dibn1s3mumphq7r8dafi9dor FOREIGN KEY (user_id)
      REFERENCES public."user" (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.auction OWNER TO postgres;


-- Table: public.tender

-- DROP TABLE public.tender;

CREATE TABLE public.tender
(
  id bigserial,
  auction_id bigint,
  created_time timestamp without time zone DEFAULT now(),
  image character varying(255),
  place character varying(255),
  price numeric(19,2),
  user_id integer,
  CONSTRAINT tender_pkey PRIMARY KEY (id),
  CONSTRAINT fk14q27axbq66ibeh5y626mwat4 FOREIGN KEY (user_id)
      REFERENCES public."user" (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk80d6voyrwq29cb115g8uqr1 FOREIGN KEY (auction_id)
      REFERENCES public.auction (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.tender OWNER TO postgres;

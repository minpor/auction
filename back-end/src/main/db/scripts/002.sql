INSERT INTO "user"(name, email, password, username, role)
VALUES ('Admin', 'mail@email.ru', '123', 'admin', 'ADMIN'),
  ('customer', 'customer@email.ru', '123', 'customer', 'CUSTOMER'),
  ('contractor', 'contractor@email.ru', '123', 'contractor', 'CONTRACTOR');

INSERT INTO auction(auction_type, budget, days, district, light, locality, size, user_id)
VALUES
  (0, 0, 1, 'ЗАО', false, 'Москва', '2x3', 1),
  (0, 1011, 1, 'ЗАО', false, 'Москва', '2x3', 1),
  (0, 1, 1, 'ЗАО', false, 'Москва', '2x3', 1),
  (0, 3, 1, 'ВАО', false, 'Москва', '3x3', 1),
  (0, 15, 1, 'ЮЗАО', false, 'Москва', '3x5', 1),
  (0, 100, 1, 'ВАО', false, 'Ростов', '3x3', 1),
  (0, 1202, 1, 'ВАО', false, 'Питер', '3x5', 1),
  (0, 15, 1, 'ЮЗАО', false, 'Москва', '3x5', 1),
  (0, 100, 5, 'ВАО', false, 'Ростов на дону', '3x3', 1),
  (0, 12202, 2, 'ВАО', false, 'Питер', '5x5', 1),
  (0, 153, 4, 'ЗАО', true, 'Москва', '7x5', 1),
  (0, 100, 50, 'ВАО', false, 'Ростов', '3x3', 1),
  (0, 100, 50, 'ВАО', false, 'ОЧЕНЬ ДЛИННОЕ НАЗВАНИЕ НАСЕЛЕННОГО ПУНКТА', '3x3', 1),
  (0, 1202, 1, 'ВАО', false, 'Питер', '3x5', 1);

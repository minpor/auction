package ru.mtauction;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.mtauction.dao.AuctionDao;
import ru.mtauction.dao.UserDao;
import ru.mtauction.domain.Auction;
import ru.mtauction.domain.enums.Role;
import ru.mtauction.domain.User;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = BackEndApplication.class)
@TestPropertySource(locations = "classpath:application.properties")
@WebAppConfiguration
public class H2Test {

	@Autowired
	private UserDao userDao;
	@Autowired
	private AuctionDao auctionDao;

	@Test
	public void userTest() {
		List<User> userList = userDao.findAll();
		assertEquals(3, userList.size());

		User admin = userDao.findOneById(1);
		assertEquals("mail@email.ru", admin.getEmail());
		assertEquals("admin", admin.getUsername());
		assertEquals(Role.ADMIN, admin.getRole());
		assertEquals("Admin", admin.getName());
	}

	@Test
	public void auctionTest() {
		List<Auction> auctionList = auctionDao.findAll();
		assertEquals(14, auctionList.size());

		Auction auction = auctionDao.findOneById(1);
		assertFalse(auction.getLight());
		assertEquals("ЗАО", auction.getDistrict());
		assertEquals("Москва", auction.getLocality());
		assertEquals("2x3", auction.getSize());
		assertEquals(new BigDecimal("0.00"), auction.getBudget());
	}
}

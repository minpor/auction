package ru.mtauction;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.web.context.WebApplicationContext;
import ru.mtauction.domain.Auction;
import ru.mtauction.domain.enums.Role;
import ru.mtauction.domain.User;
import ru.mtauction.domain.Tender;
import ru.mtauction.model.Pagination;
import ru.mtauction.model.dto.AuctionFilterDto;

import java.math.BigDecimal;
import java.nio.charset.Charset;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = BackEndApplication.class)
@TestPropertySource(locations = "classpath:application.properties")
@WebAppConfiguration
public class RestControllerTest {

	private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
			MediaType.APPLICATION_JSON.getSubtype(),
			Charset.forName("utf8"));

	private MockMvc mockMvc;

	@Autowired
	private WebApplicationContext webApplicationContext;

	@Before
	public void setUp() {
		this.mockMvc = webAppContextSetup(webApplicationContext).build();

	}


	@Test
	public void addUser() throws Exception {
		User user = new User();

		user.setId(2);
		user.setName("Name");
		user.setUsername("minpor");
		user.setPassword("ddg");
		user.setRole(Role.ADMIN);

		ObjectMapper mapper = new ObjectMapper();

		mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
		ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
		String requestJson = ow.writeValueAsString(user);

		mockMvc.perform(post("/register").contentType(contentType).content(requestJson))
				.andExpect(MockMvcResultMatchers.jsonPath("$.id").value(2))
				.andExpect(MockMvcResultMatchers.jsonPath("$.username").value("minpor"))
				.andExpect(MockMvcResultMatchers.jsonPath("$.password").value("ddg"))
				.andExpect(MockMvcResultMatchers.jsonPath("$.role").value("ADMIN"));

	}

	@Test
	public void checkAll() throws Exception {
		mockMvc.perform(get("/api/users")).andExpect(status().isOk());
	}

	@Test
	public void getAuctionListByAnyTest() {

	}

	@Test
	public void checkTenders() throws Exception {

		User user = new User();
		user.setPassword("1");
		user.setName("name");
		user.setUsername("username");
		user.setRole(Role.ADMIN);
		user.setId(3);

		Tender tender = new Tender();
		tender.setAuctionId(2);
		tender.setUserId(3);
		tender.setPlace("Улица Вилла, д 5.");
		tender.setPrice(new BigDecimal(100));

		Auction auction = new Auction();
		auction.setLight(true);
		auction.setUserId(3);


		ObjectMapper mapper = new ObjectMapper();

		mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
		ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
		String requestTenderJson = ow.writeValueAsString(tender);
		String requestAuctionJson = ow.writeValueAsString(auction);
		String requestUserJson = ow.writeValueAsString(user);

		mockMvc.perform(post("/register").contentType(contentType).content(requestUserJson))
				.andExpect(status().is2xxSuccessful());
		mockMvc.perform(post("/auctionCreate").contentType(contentType).content(requestAuctionJson))
				.andExpect(status().isOk());
		mockMvc.perform(post("/tender").contentType(contentType).content(requestTenderJson))
				.andExpect(status().isOk());

	}

	@Test
	public void checkFiler() throws Exception {
		AuctionFilterDto auction = new AuctionFilterDto();

		auction.setId(2);
		auction.setLight(false);
		auction.setPagination(new Pagination());

		ObjectMapper mapper = new ObjectMapper();

		mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
		ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
		String requestJson = ow.writeValueAsString(auction);

		mockMvc.perform(post("/auctionList")
				.contentType(contentType)
				.content(requestJson))
				.andExpect(status().isOk());
	}

}
